#! /usr/bin/env python3
"""Render openshift deployment templates."""
import argparse
import os
from jinja2 import Environment, FileSystemLoader


VAR_PREFIX = 'VAR_'
SECRET_VAR_PREFIX = 'SECRET_'


class GenericDeploy:
    """Deploy generic stuff."""

    necessary_variables = ['CI_PROJECT_NAME',]
    path = '/opt/deployment/generic'

    def __init__(self, output):
        """Init."""
        self.env = {'variables': {}, 'secret_variables': {}}
        self.output = output

    def render(self):
        """Render."""
        env = Environment(
            loader=FileSystemLoader(self.path),
            trim_blocks=True,
            lstrip_blocks=True)

        self.load_variables_from_env()
        os.mkdir(self.output)

        for file in os.listdir(self.path):
            template = env.get_template(file)
            parsed_template = template.render(**self.env)

            with open(os.path.join(self.output, file), "w") as file_handler:
                file_handler.write(parsed_template)

    @staticmethod
    def _list_variables(prefix):
        """
        Get variables from the env.

        Look for variables starting with {prefix}, remove it and yield
        the resuling key and it's value.
        """
        for key, value in os.environ.items():
            if key.startswith(prefix):
                key = key[len(prefix):]
                yield key, value

    def load_variables_from_env(self):
        """Load all variables from env."""
        for key in self.necessary_variables:
            self.env[key] = os.environ[key]

        for key, value in self._list_variables(SECRET_VAR_PREFIX):
            self.env['secret_variables'][key] = value

        for key, value in self._list_variables(VAR_PREFIX):
            self.env['variables'][key] = value


class CronDeploy(GenericDeploy):
    """Cron Job deployment."""

    necessary_variables = ['CI_PROJECT_NAME', 'cron_schedule']
    path = '/opt/deployment/cronjob'


def main(arguments):
    """Do stuff."""
    if arguments.kind == 'generic':
        deployer = GenericDeploy
    elif arguments.kind == 'cronjob':
        deployer = CronDeploy

    deployer(arguments.output).render()


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(description='Render openshift deployment config',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--output', help='Where to put the result.', default='output')
    parser.add_argument('kind', help='Kind of deployment.', choices=['cronjob'])

    return parser.parse_args()


if __name__ == '__main__':
    main(parse_args())
